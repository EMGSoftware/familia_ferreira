<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Validator;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$questions = Question::orderBy('orderLevel', 'asc')->get();

		return (empty ($request->input ('api'))) ?
					view('questions.index', compact('questions')) :
					$questions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$payload = $request->all();
		$validator = Validator::make($payload, [
            'text' => 'required',
			'orderLevel' => 'required',
        ]);

		if ($validator->fails()) 
		{
            return 'Invalid data: '.json_encode ($payload);
		}
		else
		{
			return Question::create ($payload);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }
}
