<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use Validator;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$answers = Answer::with('question')->get();
		return (empty ($request->input ('api'))) ? 
					view('answers.index', compact('answers')) :
					$answers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$payload = $request->all();
		$validator = Validator::make($payload, [
            'text' => 'required',
			'question_id' => 'required',
        ]);

		if ($validator->fails()) 
		{
            return 'Invalid data: '.json_encode ($payload);
		}
		else
		{
			Answer::create ($payload);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        //
    }
}
