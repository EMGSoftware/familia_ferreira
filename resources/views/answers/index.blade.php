@extends('welcome')
@section('content')

<table class="table table-sm table-hover table-bordered table-striped mt-4">
	<thead>
		<th>Pergunta</th>
		<th>Resposta</th>
	</thead>
	<tbody>
		@foreach($answers as $answer)
		<tr>
			<td>{{ $answer->question->text }}</td>
			<td>{{ $answer->text }}</td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection