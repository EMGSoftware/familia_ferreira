@extends('welcome')
@section('content')

<table class="table table-sm table-hover table-bordered table-striped mt-4">
	<thead>
		<th>Pergunta</th>
		<th>Ordem</th>
	</thead>
	<tbody>
		@foreach($questions as $question)
		<tr>
			<td>{{ $question->text }}</td>
			<td>{{ $question->orderLevel }}</td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection